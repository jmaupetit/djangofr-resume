#!/usr/bin/env python

import codecs
import datetime
import json
import locale
import logging
import os.path
import re
import sys
import unicodedata

from md2pdf import md2pdf

logging.basicConfig(level=logging.DEBUG)


def slugify(value):
    """
    Taken from django.utils.text

    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')  # NOPEP8
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)


def main(argv=None):

    # Load project configuration
    logging.info("Read configuration...")
    json_file = open('config.json', 'r')
    config = json.load(json_file)
    json_file.close()

    # Add last_modification to context
    now = datetime.datetime.now()
    locale.setlocale(locale.LC_ALL, 'fr_FR')  # Set locale to french
    config['context']['last_modification'] = now.strftime(u'%c')

    # Concatenate markdown files
    logging.info("Concatenate files...")
    md_cat = u""
    for md_file in config.get('files', None):
        logging.debug(md_file)
        md_cat += u"".join(codecs.open(md_file, encoding='utf-8').readlines())

    # Compile template
    logging.info("Compile templates...")
    template_vars = config['context'].keys()
    for template_var in template_vars:
        logging.debug(template_var)
        pattern = u"{{ %(var)s }}" % {'var': template_var}
        repl = config['context'][template_var]
        md_cat = re.sub(pattern, repl, md_cat)

    # Write output file
    pdf_output = u"%(ref)s.pdf" % {
        'ref': config['context']['reference']
    }
    base_url = "file://"
    base_url += os.path.realpath(__file__)
    logging.info("Write output file: %s", pdf_output)
    md2pdf(
        pdf_output,
        md_content=md_cat,
        css_file_path=config.get('css', None),
        base_url=base_url
    )

    return 1


if __name__ == '__main__':
    sys.exit(main())
