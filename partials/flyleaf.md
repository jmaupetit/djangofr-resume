<header>

    <h1>DjangoCong 2015</h1>

    <h2>Clermont-Ferrand</h2>

    <h3>8-10 Mai 2015</h3>

    <div class="logo">
        <img src="images/logo.png">
    </div>

    <div class="info">
        Date : {{ date }} <br>
        Dernière modification : {{ last_modification }}
    </div>

</header>
