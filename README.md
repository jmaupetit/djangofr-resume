# DjangoFR document

Generate beautiful documents with Markdown and Sass.

## Dependencies

### md2pdf

[md2pdf](https://github.com/jmaupetit/md2pdf) is used as a library to assemble and compile your document from markdown (and css) files to a rendered/styled html file converted to a single PDF file.

### sass

If you which to update styles, [install sass](http://sass-lang.com/install) first.

## Usage

### Document

You need first to configure your project settings by editing the `config.json` file.

Then if you already installed [md2pdf](https://github.com/jmaupetit/md2pdf), compile your document with the following command:

    $ python make.py

### Styles

Once you have modified your sass styles, compile it with:

    $ sass sass/djangofr.scss stylesheets/djangofr.css
